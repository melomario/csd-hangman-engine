const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    expect(2).toBe(2);
  });

  test("update the guesses and lives when a wrong guess is given", () => {
    expect(1).toBe(1);
  });
});

